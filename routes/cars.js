const express = require('express');
const router = express.Router();
const models = require('../models');
const form = require('express-form');
const field = form.field;

// Formulaire de création
router.get('/create', function(req, res, next) {
	models
		.Brand
		.findAll()
		.then(brands => {
			res.render('cars/create', {
				brands: brands,
			});
		})
	;
});

function createForm() {
	return form(
		field("name").trim().required(),
		field("engine").trim().required(),
		field("color").trim().required(),
		field("fuel").trim().required(),
		field("model").trim().required(),
		field("brandId").trim().required(),
	);
}

router.post('/create', createForm(), function(req, res, next) {
	if (req.form.isValid) {
		models
			.Car
			.create({
				name: req.form.name,
				engine: req.form.engine,
				color: req.form.color,
				fuel: req.form.fuel,
				model: req.form.model,
				brandId: req.form.brandId,
			}).then(() => {
				res.redirect('/cars');
			})
		;
	}
});


router.get('/', function(req, res, next) {
	models
		.Car
		.findAll({
			limit: 10,
			include: ['brand']
		}).then(cars => {
			res.render('cars/index', {
				cars: cars
			});
		})
	;
});

router.get('/:id/view', function(req, res, next) {
	models
		.Car
		.findOne({
			where: { 
				id: req.params.id
			},
			include: ['brand']
		}).then(car => {
			res.render('cars/view', {
				car: car
			});
		})
	;
});

router.get('/:id/remove', function(req, res, next) {
	models
		.Car
		.destroy({
			where: { 
				id: req.params.id
			}
		}).then(car => {
			res.redirect('/cars');
		})
	;
});

module.exports = router;
