module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Cars', // name of Source model
      'brandId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        references: {
          model: 'Brands',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Cars', 'brandId'
    );
  }
};